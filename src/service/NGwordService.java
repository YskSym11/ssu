package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import dao.NGwordDao;

public class NGwordService {

	public List<String> select() {
		Connection connection = null;
		try {
			connection = getConnection();
			List<String> ngwords = new NGwordDao().select(connection);
			commit(connection);

			return ngwords;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

}
