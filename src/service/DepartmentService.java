package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Department;
import dao.DepartmentDao;

public class DepartmentService {

	public List<Department> select() {

        Connection connection = null;
        try {
            connection = getConnection();
            List<Department> department = new DepartmentDao().select(connection);
            commit(connection);

            return department;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public Department select(int id) {

        Connection connection = null;
        try {
            connection = getConnection();
            Department department = new DepartmentDao().select(connection, id);
            commit(connection);

            return department;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}