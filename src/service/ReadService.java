package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.ReadDao;

public class ReadService {

	public void insert(int userId, int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			new ReadDao().insert(connection, userId, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public int select(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            int readCount = new ReadDao().select(connection, messageId);
            commit(connection);

            return readCount;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}