package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exception.SQLRuntimeException;

public class NGwordDao {

	public List<String> select(Connection connection) {

		PreparedStatement fc = null;
		List<String> ngwords = new ArrayList<String>();
        PreparedStatement ps = null;
        try {
            String fcsql = "SELECT * FROM ngwords";
			fc = connection.prepareStatement(fcsql);
            ResultSet rs = fc.executeQuery(fcsql);

            while(rs.next()) {
            	String word = rs.getString("word");
            	ngwords.add(word);
            }

           return ngwords;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
