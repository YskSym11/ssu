package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    users.id as id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    branches.name as branch, ");
			sql.append("    departments.name as department, ");
			sql.append("    users.is_stopped as is_stopped ");
			sql.append("FROM users ");
			sql.append("INNER JOIN  branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN  departments ");
			sql.append("ON  users.department_id  = departments.id ");
			sql.append("    ORDER BY users.created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> userInfos = toUserBranchDepartment(rs);
			return userInfos;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	private List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> userInfos = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment userInfo = new UserBranchDepartment();
				userInfo.setId(rs.getInt("id"));
				userInfo.setAccount(rs.getString("account"));
				userInfo.setName(rs.getString("name"));
				userInfo.setBranch(rs.getString("branch"));
				userInfo.setDepartment(rs.getString("department"));
				userInfo.setIsStopped(rs.getInt("is_stopped"));

				userInfos.add(userInfo);
			}
			return userInfos;
		} finally {
			close(rs);
		}
	}
}