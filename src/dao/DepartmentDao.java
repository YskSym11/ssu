package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Department> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM departments";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Department> departments = toDepartment(rs);
			return departments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Department select(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM departments WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<Department> departments = toDepartment(rs);
			if (departments.isEmpty()) {
                return null;
            } else if (2 <= departments.size()) {
                throw new IllegalStateException("部署が重複しています");
            } else {
                return departments.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	private List<Department> toDepartment(ResultSet rs) throws SQLException {

		List<Department> departments = new ArrayList<Department>();
		try {
			while (rs.next()) {
				Department department = new Department();
				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));
				department.setCreatedDate(rs.getTimestamp("created_date"));
				department.setUpdatedDate(rs.getTimestamp("updated_date"));

				departments.add(department);
			}
			return departments;
		} finally {
			close(rs);
		}
	}
}
