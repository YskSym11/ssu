package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Read;
import exception.SQLRuntimeException;

public class ReadDao {

	public void insert(Connection connection, int userId, int messageId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO is_reads ( ");
			sql.append("    user_id, ");
			sql.append("    messageId, ");
			sql.append("    is_read, ");
			sql.append("    created_date, ");
			sql.append(") VALUES ( ");
			sql.append("    ?, ");
			sql.append("    ?, ");
			sql.append("    1, ");
			sql.append("    CURRENT_TIMESTAMP ");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, userId);
			ps.setInt(2, messageId);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public int select(Connection connection, int messageId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE message_id = ? AND is_read = 1";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, messageId);

            ResultSet rs = ps.executeQuery();

            List<Read> readCount = toReads(rs);
            return readCount.size();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Read> toReads(ResultSet rs) throws SQLException {

		List<Read> reads = new ArrayList<Read>();
		try {
			while (rs.next()) {
				Read read = new Read();
				read.setId(rs.getInt("id"));
				read.setUserId(rs.getInt("user_id"));
				read.setUserId(rs.getInt("message_id"));
				read.setUserId(rs.getInt("is_read"));
				read.setCreatedDate(rs.getTimestamp("created_date"));

				reads.add(read);
			}
			return reads;
		} finally {
			close(rs);
		}
	}
}