package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Branch> branches = toBranch(rs);
			return branches;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Branch select(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<Branch> branches = toBranch(rs);
			if (branches.isEmpty()) {
                return null;
            } else if (2 <= branches.size()) {
                throw new IllegalStateException("支社が重複しています");
            } else {
                return branches.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<Branch> toBranch(ResultSet rs) throws SQLException {

		List<Branch> branches = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				Branch branch = new Branch();
				branch.setId(rs.getInt("id"));
				branch.setName(rs.getString("name"));
				branch.setCreatedDate(rs.getTimestamp("created_date"));
				branch.setUpdatedDate(rs.getTimestamp("updated_date"));

				branches.add(branch);
			}
			return branches;
		} finally {
			close(rs);
		}
	}
}
