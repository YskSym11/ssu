package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;
import service.NGwordService;

@WebServlet("/comment")
public class CommentServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        String text = request.getParameter("text");
        int id = Integer.parseInt(request.getParameter("id"));
        List <String>ngwords = new NGwordService().select();

        if (!isValid(text, errorMessages,ngwords)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
        }

        Comment comment = new Comment();
        comment.setText(text);
        comment.setMessageId(id);

        User user = (User) session.getAttribute("loginUser");
        comment.setUserId(user.getId());
        new CommentService().insert(comment);
        response.sendRedirect("./");
	}
	private boolean isValid(String text, List<String> errorMessages,List<String> ngwords) {

		int size = ngwords.size();

		for(int i = 0 ; i < size; i++ ) {
			if(text.contains(ngwords.get(i))) {
				errorMessages.add("不適切な表現が含まれています。");
			}
		}
    	if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (500 < text.length()) {
            errorMessages.add("500文字以下で入力してください");
        }


        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
