package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.ReadService;

@WebServlet(urlPatterns = { "/read" })
public class ReadServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		User user = (User)(User) session.getAttribute("loginUser");

		int userId = user.getId();
		int messageId = Integer.parseInt(request.getParameter("id"));

		new ReadService().insert(userId, messageId);

		response.sendRedirect("./");

	}
}