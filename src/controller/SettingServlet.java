package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String Id = request.getParameter("id");
		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (StringUtils.isBlank(Id) || !Id.matches("^[0-9]+$")) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./management");
			return;
		}

		int id = Integer.parseInt(Id);

		User user = new UserService().select(id);
		List<Branch> branches = new BranchService().select();
    	List<Department> departments = new DepartmentService().select();

    	if (user == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./management");
			return;
		}
    	int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        Branch branch = new BranchService().select(branchId);
        Department department = new DepartmentService().select(departmentId);

    	request.setAttribute("branches", branches);
    	request.setAttribute("departments", departments);
    	request.setAttribute("branchName", branch);
    	request.setAttribute("departmentName", department);
		request.setAttribute("user", user);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);

		String checkAccount = user.getAccount();
		int userId = user.getId();

        User accountCheck = new UserService().select(checkAccount);
        User idCheck= new UserService().select(userId);
        int id = idCheck.getId();


		if (isValid(user, accountCheck, id, errorMessages)) {
			try {
				new UserService().update(user);
			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		if (errorMessages.size() != 0) {
			List<Branch> branches = new BranchService().select();
	    	List<Department> departments = new DepartmentService().select();

	    	request.setAttribute("errorMessages", errorMessages);
	    	request.setAttribute("branches", branches);
	    	request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}
		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("id")));
        user.setName(request.getParameter("name"));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setConfirm(request.getParameter("confirm"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        return user;
    }

	private boolean isValid(User user, User accountCheck, int id, List<String> errorMessages) {

    	String name = user.getName();
        String account = user.getAccount();
        String password = user.getPassword();
        String confirm = user.getConfirm();
        int branch = user.getBranchId();
        int department = user.getDepartmentId();

        if(StringUtils.isBlank(name)) {
        	errorMessages.add("ユーザー名を入力してください");
        }

        if (!StringUtils.isBlank(name) && (10 < name.length())) {
            errorMessages.add("ユーザー名は10文字以下で入力してください");
        }

        if (StringUtils.isBlank(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if(account.length() < 6) {
            errorMessages.add("アカウント名は6文字以上で入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        }

        if((accountCheck != null) && (id != accountCheck.getId())) {
        	errorMessages.add("アカウントが重複しています");
		}

        if (StringUtils.isBlank(password)) {
            errorMessages.add("パスワードを入力してください");
        }else if(password.length()< 6) {
        	errorMessages.add("パスワードは6文字以上で入力してください");
        }else if(20 < password.length()) {
        	errorMessages.add("パスワードは20文字以下で入力してください");
        }

        if(!password.equals(confirm)) {
        	errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
        }

        if(branch == 1) {
        	errorMessages.add("支社を選択してください");
        }

        if(department == 1) {
        	errorMessages.add("部署を選択してください");
        }

        if(branch == 2 && department > 3) {
        	errorMessages.add("支社と部署の組み合わせが間違っています");
        }

        if(branch > 2 && department < 4 ) {
        	errorMessages.add("支社と部署の組み合わせが間違っています");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}