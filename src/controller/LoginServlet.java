package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
    	List<String> errorMessages = new ArrayList<String>();

        String account = request.getParameter("account");
        String password = request.getParameter("password");

        if(!isValidInput(errorMessages, account, password)) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("account", account);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        User user = new UserService().select(account, password);

        if(!isValidCheck(user, errorMessages, account, password)) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("account", account);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        HttpSession session = request.getSession();
        session.removeAttribute("errorMessages");
        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }

    private boolean isValidInput(List<String> errorMessages, String account, String password) {

    	if(StringUtils.isBlank(account)) {
    		errorMessages.add("アカウント名を入力してください。");
        }

    	if(StringUtils.isBlank(password)) {
    		errorMessages.add("パスワードを入力してください。");
        }

    	if (errorMessages.size() != 0) {
            return false;
        }
        return true;

    }

    private boolean isValidCheck(User user, List<String> errorMessages, String account, String password) {

    	if(user == null) {
    		errorMessages.add("アカウントまたはパスワードが間違っています。");
    		return false;
    	}

    	int isStopped = user.getIsStopped();

    	if(isStopped == 1) {
    		errorMessages.add("アカウントまたはパスワードが間違っています。");
    	}

    	if (errorMessages.size() != 0) {
            return false;
        }
        return true;

    }
}