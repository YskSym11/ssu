package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;
import service.NGwordService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("/message.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        String title = request.getParameter("title");
        String category = request.getParameter("category");
        String text = request.getParameter("text");

        Message message = new Message();
        message.setTitle(title);
        message.setCategory(category);
        message.setText(text);

        List <String>ngwords = new NGwordService().select();

        if (!isValid(title, category, text, errorMessages, ngwords)) {
            session.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", message);
            request.getRequestDispatcher("message.jsp").forward(request, response);
            return;
        }

        if (!isValid(title, category, text, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", message);
            request.getRequestDispatcher("message.jsp").forward(request, response);
            return;
        }

        User user = (User) session.getAttribute("loginUser");
        message.setUserId(user.getId());

        new MessageService().insert(message);

        response.sendRedirect("./");

	}

	private boolean isValid(String title,  String category, String text,List<String> errorMessages) {

    	if (StringUtils.isBlank(title)) {
            errorMessages.add("件名が未入力のまま投稿することは出来ません。");
        } else if (30 < title.length()) {
            errorMessages.add("件名は30文字以下で入力してください");
        }

    	if (StringUtils.isBlank(category)) {
            errorMessages.add("カテゴリーが未入力のまま投稿することは出来ません。");
        } else if (10 < category.length()) {
            errorMessages.add("カテゴリーは10文字以下で入力してください");
        }

    	if (StringUtils.isBlank(text)) {
            errorMessages.add("投稿内容が未入力のまま投稿することは出来ません。");
        } else if (1000 < text.length()) {
            errorMessages.add("投稿内容は1000文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

	private boolean isValid(String title,  String category, String text,List<String> errorMessages, List<String> ngwords) {

		int size = ngwords.size();

		for(int i = 0 ; i < size; i++ ) {
			if(title.contains(ngwords.get(i))){
				errorMessages.add("件名に不適切な表現が含まれています。");
			}
			if(category.contains(ngwords.get(i))) {
				errorMessages.add("カテゴリーに不適切な表現が含まれています。");
			}
			if(text.contains(ngwords.get(i))) {
				errorMessages.add("投稿内容に不適切な表現が含まれています。");
			}
		}
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}
