package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBranchDepartment;
import service.UserService;



@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<UserBranchDepartment> userInfos = new UserService().select();

		request.setAttribute("userInfos", userInfos);
		request.getRequestDispatcher("management.jsp").forward(request, response);

	}

}
