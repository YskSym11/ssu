package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(filterName="authorityFilter", urlPatterns = { "/management", "/signup", "/setting" })
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession(false);
		User loginUser = (User) session.getAttribute("loginUser");

		int branchId = loginUser.getBranchId();
		int departmentId = loginUser.getDepartmentId();


		if (branchId == 2 && departmentId == 2) {
			chain.doFilter(request, response);

		} else {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("権限がありません");
			session.setAttribute("errorMessages", errorMessages);

			((HttpServletResponse) response).sendRedirect("./");
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}