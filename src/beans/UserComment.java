package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {

    private int id;
    private String account;
    private String name;
    private int messageId;
    private int userId;
    private int branchId;
    private int departmentId;
    public int getId() {
		return id;
	}
	public String getAccount() {
		return account;
	}
	public String getName() {
		return name;
	}
	public int getUserId() {
		return userId;
	}
	public String getText() {
		return text;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setText(String text) {
		this.text = text;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

	private String text;
    private Date createdDate;

    // getter/setterは省略されているので、自分で記述しましょう。

}
