<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/jQuery.min.js"></script>
<script type="text/javascript" src="./js/javascript.js"></script>
<title>ユーザー新規登録</title>
</head>
<body>
    <div class="header">
		<a href="management">ユーザー管理</a>
	</div>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <form action="signup" method="post"><br />
                <label for="account">アカウント名</label>
                <input name="account" value="${user.account}" id="account" /><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="confirm">パスワード（確認用）<span id="result"></span></label>
                <input name="confirm" type="password" id="confirm"/> <br />

                <label for="name">名前</label>
                <input name="name" value="${user.name}" id="name" /> <br />

                <label for="branch">支社</label>
                <select name="branchId" id="department">

	                <c:forEach var="branch" items="${branches}">
	                <c:if test="${ user.branchId == branch.id }">
	                	<option value="${branch.id}" selected><c:out value="${branch.name}" /></option>
	                </c:if>
	                <c:if test="${ user.branchId != branch.id }">
	                	<option value="${branch.id}"><c:out value="${branch.name}" /></option>
	                </c:if>
	                </c:forEach>
                </select>

                <label for="department">部署</label>
                <select name="departmentId" id="department" >
	                <c:forEach var="department" items="${departments}">
	                	<c:if test="${ user.departmentId == department.id }">
	                		<option value="${department.id}" selected><c:out value="${department.name}" /></option>
	                	</c:if>
	                	<c:if test="${ user.departmentId != department.id }">
	                		<option value="${department.id}"><c:out value="${department.name}" /></option>
	                	</c:if>
	                </c:forEach>
                </select>

                <input class="button" type="submit" value="登録" /> <br />
            </form>

            <div class="copyright">Copyright(c)SimpleServiceUnit</div>
        </div>
</body>
</html>