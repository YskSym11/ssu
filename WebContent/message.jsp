<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/jQuery.min.js"></script>
<script type="text/javascript" src="./js/javascript.js"></script>
<title>ssu</title>
</head>
<body>
	<a href="./">ホーム</a>
	<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                        <c:remove var="errorMessages" scope="session" />
                    </ul>
                </div>
            </c:if>

	<div class="form-area">
		<div class="message-form">
			<form action="message" method="post">
				<label for="title">件名 (30文字まで)</label>
				<input name="title" id="title" value="${message.title}"/><br />
				<label for="category">カテゴリー (10文字まで)</label>
				<input name="category" id="category" value="${message.category}"/> <br />
				<label for="text">投稿内容</label>
				<textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value="${message.text}" /></textarea><br />
				<input  class="button" type="submit" value="投稿">（1000文字まで）<br />
			</form>
		</div>
	</div>
	<div class="copyright">Copyright(c)SimpleServiceUnit</div>

</body>
</html>