<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.4/css/all.css">

<title>ホーム</title>
<script type="text/javascript" src="./js/jQuery.min.js"></script>
<script type="text/javascript" src="./js/javascript.js"></script>


</head>



<body>


	<div class="header">
		<a href="message">新規投稿</a>
		<c:if test="${ loginUser.departmentId ==  2}">
			<a href="management">ユーザー管理</a>
		</c:if>

		<a href="logout">ログアウト</a>
	</div>
	<div class="main-contents">


		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					<c:out value="${loginUser.account}" />
				</div>
				<div class="lastDate">
					最終アクセス日：<c:out value="${lastDate}" />
				</div>
			</div>
		</c:if>


		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="" method="get">
			<div class="narrow-down">
				日付： <input type="date" name="start-date" id="start-date"
					value="${startDate}" /> ～ <input type="date" name="end-date"
					id="end-date" value="${endDate}" /><br> カテゴリー <input
					name="category" type="text" id="category" value="${category }">
				<input class="button" type="submit" value="絞り込み">
				<input type="button" value="絞り込み解除" onClick="location.href='./'" />			</div>
		</form>
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="name">
						<span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="title">
						<span class="title"><c:out value="${message.title}" /></span>
					</div>
					<div class="category">
						<span class="category">Category: #<c:out value="${message.category}" /></span>
					</div>
					<div class="text">
						<span style="white-space: pre-wrap"><c:out
								value="${message.text}" /></span>
					</div>
					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>

					<div class="buttons">
						<div style="display: inline-flex">

							<form action="deleteMessage" method="post"
								onSubmit="return check()">
								<c:if test="${ loginUser.departmentId == 3 }">
									<input name="id" value="${message.id}" id="id" type="hidden" />
									<input class="button" type="submit" value="削除">
								</c:if>
								<c:if
									test="${ loginUser.branchId == message.branchId and loginUser.departmentId == 4 and  message.departmentId == 5 }">
									<input name="id" value="${message.id}" id="id" type="hidden" />
									<input class="button" type="submit" value="削除">
								</c:if>
								<c:if
									test="${ loginUser.departmentId !=3 and loginUser.id == message.userId}">
									<input name="id" value="${message.id}" id="id" type="hidden" />
									<input class="button" type="submit" value="削除">
								</c:if>
							</form>
						</div>
					</div>
				</div>

				<c:forEach items="${comments}" var="comment">
					<c:if test="${ message.id == comment.messageId }">
						<div class="comment">
							<div class="account-name">
								<span class="account"><c:out value="${comment.account}" /></span>
								<span class="name"><c:out value="${comment.name}" /></span>
							</div>
							<div class="text">
								<span style="white-space: pre"><c:out
										value="${comment.text}" /></span>
							</div>
							<div class="date">
								<fmt:formatDate value="${comment.createdDate}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>

							<form action="deleteComment" method="post"
								onSubmit="return check()">
								<c:if test="${ loginUser.departmentId == 3 }">
									<input name="id" value="${comment.id}" id="id" type="hidden" />
									<input class="button" type="submit" value="削除">
								</c:if>
								<c:if
									test="${ loginUser.branchId == comment.branchId and loginUser.departmentId == 4 and  comment.departmentId == 5 }">
									<input name="id" value="${comment.id}" id="id" type="hidden" />
									<input class="button" type="submit" value="削除">
								</c:if>
								<c:if
									test="${ loginUser.departmentId !=3 and loginUser.id == comment.userId}">
									<input name="id" value="${comment.id}" id="id" type="hidden" />
									<input class="button" type="submit" value="削除">
								</c:if>
							</form>
						</div>
					</c:if>
				</c:forEach>
				<div class="comment-box">
					<div class="comment-zone">
						<form action="comment" method="post">
							コメント入力欄<br /> <input name="id" value="${message.id}" id="id"
								type="hidden" />
							<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
							<br /> <input class="button" type="submit" value="コメント">（500文字まで）
						</form>
					</div>
				</div>
			</c:forEach>

		</div>
		<p id="topbutton">
			<a href="#top"
				onclick="$('html,body').animate({ scrollTop: 0 }); return false;">▲TOPへ戻る</a>
		</p>



		<div class="copyright">Copyright(c)SimpleServiceUnit</div>

	</div>

</body>

</html>