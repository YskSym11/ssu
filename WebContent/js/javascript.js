function check(){
	if(window.confirm('削除してよろしいですか？')){ // 確認ダイアログを表示
		window.alert('投稿を削除しました');
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}
}

function stopCheck(){
      var checked = confirm("アカウントを停止します。よろしいですか？");
      if (checked == true) {
          return true;
      } else {
    	  window.alert('キャンセルされました');
          return false;
      }
  }

function rebootCheck(){
    var checked = confirm("アカウントを復活します。よろしいですか？");
    if (checked == true) {
        return true;
    } else {
    	window.alert('キャンセルされました');
        return false;
    }
}

$(function() {
	$('#confirm').on('input', function() {
		$('#result').html(checkPassword)
	});
	function checkPassword() {
		var input1 = $('#password').val();
		var input2 = $('#confirm').val();
		if (input1 != input2) {
			return "パスワードが一致しません。";
		} else {
			return "";
		}
	}
});

$(function(){
	  $('.button').hover(function(){
		  $(this).css('color', '#5bc6ff').css('background-color', '#fff');
	  },function(){
		  $(this).css('color', '#fff').css('background-color', '#5bc6ff');
	  });
});
