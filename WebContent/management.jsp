<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/jQuery.min.js"></script>
<script type="text/javascript" src="./js/javascript.js"></script>
<title>ユーザー管理</title>
</head>
<body>
	<div class="header">
		<a href="./">ホーム</a>
		<a href="signup">ユーザー新規登録</a>
	</div>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<div class="main-contents">
		<c:forEach items="${userInfos}" var="userInfo">
		<div class="user-management">
			<div class="userInfo">
				<input type="hidden" value="${userInfo.id}">
				<div class="account">
				<div class="font">
					<c:out value="${userInfo.account}" />
				</div>
				</div>
				<div class="name">
					<c:out value="${userInfo.name}" />
				</div>
				<div class="branch">
					<c:out value="${userInfo.branch}" />
				</div>
				<div class="department">
					<c:out value="${userInfo.department}" />
				</div>
				<div class="department">
					<c:if test="${userInfo.isStopped == 0}">
						<c:out value="稼働中" />
					</c:if>
					<c:if test="${userInfo.isStopped == 1}">
						<c:out value="停止中" />
					</c:if>
				</div>
			</div>
			</div>

			<div style="display: inline-flex">
				<form action="setting" method="get">
					<input type="hidden" name="id" value="${userInfo.id}" />
					<input class="btn btn--orange btn--cubic btn--shadow" type="submit" value="編集"/>
				</form><c:if test="${loginUser.id != userInfo.id}">
					<form action="stop" method="post">
						<c:if test="${userInfo.isStopped == 0}">
							<input type="hidden" value="${userInfo.id}" name="id" />
							<input type="hidden" value="1" name="isStopped" />
							<input class="btn btn--orange btn--cubic btn--shadow" type="submit" value="停止" onClick="return stopCheck()"/>
						</c:if>
						<c:if test="${userInfo.isStopped == 1}">
							<input type="hidden" value="${userInfo.id}" name="id" />
							<input type="hidden" value="0" name="isStopped"/>
							<input class="btn btn--orange btn--cubic btn--shadow"  type="submit" value="復活" onClick="return rebootCheck()"/>
						</c:if>
					</form>
				</c:if>
				</div>
		</c:forEach>
		<div class="copyright"> Copyright(c)SimpleServiceUnit</div>
	</div>
</body>
</html>