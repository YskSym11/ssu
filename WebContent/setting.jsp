<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.account}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/jQuery.min.js"></script>
<script type="text/javascript" src="./js/javascript.js"></script>
</head>
<body>
	<div class="header">
		<a href="management">ユーザー管理</a>
	</div>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
            <form action="setting" method="post"><br />
                <input name="id" value="${user.id}" id="id" type="hidden"/>

                <label for="account">アカウント名</label>
                <input name="account" value="${user.account}" /><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="confirm">パスワード（確認用）<span id="result"></span></label>
                <input name="confirm" type="password" id="confirm"/> <br />

                <label for="name">名前</label>
                <input name="name" value="${user.name}" id="name"/> <br />


                <label for="branch">支社</label>
                <c:if test="${ loginUser.account == user.account }">
                	<c:out value="${ branchName.name }" />
                	<input type="hidden" name="branchId" value="${branchName.id}" />
                </c:if>
                <c:if test="${ loginUser.account != user.account }">
                <select name="branchId" id="department">

	                <c:forEach var="branch" items="${branches}">
	                <c:if test="${ user.branchId == branch.id }">
	                	<option value="${branch.id}" selected><c:out value="${branch.name}" /></option>
	                </c:if>
	                <c:if test="${ user.branchId != branch.id }">
	                	<option value="${branch.id}"><c:out value="${branch.name}" /></option>
	                </c:if>
	                </c:forEach>
                </select>
                </c:if>

                <label for="department">部署</label>
                <c:if test="${ loginUser.account == user.account }">
                	<c:out value="${ departmentName.name }" />
                	<input type="hidden" name="departmentId" value="${departmentName.id}" />
                </c:if>
                <c:if test="${ loginUser.account != user.account }">
                <select name="departmentId" id="department" >
	                <c:forEach var="department" items="${departments}">
	                	<c:if test="${ user.departmentId == department.id }">
	                		<option value="${department.id}" selected><c:out value="${department.name}" /></option>
	                	</c:if>
	                	<c:if test="${ user.departmentId != department.id }">
	                		<option value="${department.id}"><c:out value="${department.name}" /></option>
	                	</c:if>
	                </c:forEach>
                </select>
                </c:if>

                <input class="button" type="submit" value="更新" /> <br />
            </form>

            <div class="copyright"> Copyright(c)SympleServiceUnit</div>
        </div>
    </body>
</html>